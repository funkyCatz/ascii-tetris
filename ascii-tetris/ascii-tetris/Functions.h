#ifndef __ASCII_TETRIS_FUNCTIONS__30032020__
#define __ASCII_TETRIS_FUNCTIONS__30032020__

#include "Configuration.h"
#include <memory>

namespace  functions 
{
	int Rotate(int px, int py, int rotation);
	bool DoesFigureFit(const Configuration& cfg, const std::unique_ptr<unsigned char[]>& pTable, size_t nFigureId, int nRotation, int nPosX, int nPosY);
}

#endif



