#include <iostream>
#include <Windows.h>
#include "Configuration.h"
#include <thread>
#include <vector>
#include <chrono>
#include "Functions.h"

using namespace functions;

int main()
{
	Configuration cfg;
	cfg.SetupAssets();

	const std::unique_ptr<unsigned char[]> pTable(new unsigned char[cfg.nFieldWidth * cfg.nFieldHeight]);

	for (int x = 0; x < cfg.nFieldWidth; x++)
	{
		for (int y = 0; y < cfg.nFieldHeight; y++)
		{
			pTable[y * cfg.nFieldWidth + x] = (x == 0 || x == cfg.nFieldWidth - 1 || y == cfg.nFieldHeight - 1) ? 9 : 0;
		}
	}

	const std::unique_ptr<wchar_t[]> pScreen(new wchar_t[cfg.nScreenHeight * cfg.nScreenWidth]);
	for (int i = 0; i < cfg.nScreenHeight * cfg.nScreenWidth; i++)
	{
		pScreen[i] = L' ';
	}

	HANDLE hConsole = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 
		0, nullptr,CONSOLE_TEXTMODE_BUFFER, nullptr);
	SetConsoleActiveScreenBuffer(hConsole);
	DWORD dwWrittenBytes = 0;

	bool bKey[4];
	int nCurrentPiece = 0;
	int nCurrentRotation = 0;
	int nCurrentX = cfg.nFieldWidth / 2;
	int nCurrentY = 0;
	int nSpeed = 20;
	int nSpeedCount = 0;
	bool bRotateHold = true;
	int nPieceCount = 0;
	int nScore = 0;
	std::vector<int> vecLines;
	bool bGameOver = false;

	while (!bGameOver)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(50)); 
		nSpeedCount++;
		const bool bForceDown = (nSpeedCount == nSpeed);

		for (int k = 0; k < 4; k++)								// R   L   D Z
			bKey[k] = (0x8000 & GetAsyncKeyState(static_cast<unsigned char>("\x27\x25\x28Z"[k]))) != 0;

		nCurrentX += (bKey[0] && DoesFigureFit(cfg, pTable, nCurrentPiece, nCurrentRotation, nCurrentX + 1, nCurrentY)) ? 1 : 0;
		nCurrentX -= (bKey[1] && DoesFigureFit(cfg, pTable, nCurrentPiece, nCurrentRotation, nCurrentX - 1, nCurrentY)) ? 1 : 0;
		nCurrentY += (bKey[2] && DoesFigureFit(cfg, pTable, nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY + 1)) ? 1 : 0;

		if (bKey[3])
		{
			nCurrentRotation += (bRotateHold && DoesFigureFit(cfg, pTable, nCurrentPiece, nCurrentRotation + 1, nCurrentX, nCurrentY)) ? 1 : 0;
			bRotateHold = false;
		}
		else
			bRotateHold = true;

		if (bForceDown)
		{
			nSpeedCount = 0;
			nPieceCount++;
			if (nPieceCount % 50 == 0)
				if (nSpeed >= 10) nSpeed--;

			if (DoesFigureFit(cfg, pTable, nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY + 1))
				nCurrentY++; 
			else
			{
				for (int px = 0; px < 4; px++)
					for (int py = 0; py < 4; py++)
						if (cfg.shapes[nCurrentPiece][Rotate(px, py, nCurrentRotation)] != L'.')
							pTable[(nCurrentY + py) * cfg.nFieldWidth + (nCurrentX + px)] = nCurrentPiece + 1;

				for (int py = 0; py < 4; py++)
					if (nCurrentY + py < cfg.nFieldHeight - 1)
					{
						bool bLine = true;
						for (int px = 1; px < cfg.nFieldWidth - 1; px++)
							bLine &= (pTable[(nCurrentY + py) * cfg.nFieldWidth + px]) != 0;

						if (bLine)
						{
							for (int px = 1; px < cfg.nFieldWidth - 1; px++)
								pTable[(nCurrentY + py) * cfg.nFieldWidth + px] = 8;
							vecLines.push_back(nCurrentY + py);
						}
					}

				nScore += 25;
				if (!vecLines.empty())	nScore += (1 << vecLines.size()) * 100;

				nCurrentX = cfg.nFieldWidth / 2;
				nCurrentY = 0;
				nCurrentRotation = 0;
				nCurrentPiece = rand() % 7;

				bGameOver = !DoesFigureFit(cfg, pTable, nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY);
			}
		}

		for (int x = 0; x < cfg.nFieldWidth; x++)
			for (int y = 0; y < cfg.nFieldHeight; y++)
				pScreen[(y + 2) * cfg.nScreenWidth + (x + 2)] = L" ABCDEFG=#"[pTable[y * cfg.nFieldWidth + x]];

		for (int px = 0; px < 4; px++)
			for (int py = 0; py < 4; py++)
				if (cfg.shapes[nCurrentPiece][Rotate(px, py, nCurrentRotation)] != L'.')
					pScreen[(nCurrentY + py + 2) * cfg.nScreenWidth + (nCurrentX + px + 2)] = nCurrentPiece + 65;

		swprintf_s(&pScreen[2 * cfg.nScreenWidth + cfg.nFieldWidth + 6], 16, L"SCORE: %8d", nScore);

		if (!vecLines.empty())
		{
			WriteConsoleOutputCharacter(hConsole, pScreen.get(), cfg.nScreenWidth * cfg.nScreenHeight, { 0,0 }, &dwWrittenBytes);
			std::this_thread::sleep_for(std::chrono::milliseconds(400));

			for (auto& v : vecLines)
				for (int px = 1; px < cfg.nFieldWidth - 1; px++)
				{
					for (int py = v; py > 0; py--)
						pTable[py * cfg.nFieldWidth + px] = pTable[(py - 1) * cfg.nFieldWidth + px];
					pTable[px] = 0;
				}

			vecLines.clear();
		}

		WriteConsoleOutputCharacter(hConsole, pScreen.get(), cfg.nScreenWidth * cfg.nScreenHeight, { 0,0 }, &dwWrittenBytes);
	}

	CloseHandle(hConsole);
	std::cout << "GaMe OvEr... Your score:" << nScore << std::endl;
	return 0;
}