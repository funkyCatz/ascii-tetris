#include "Configuration.h"

Configuration::Configuration()
{
	nScreenWidth = 80;
	nScreenHeight = 30;
	nFieldHeight = 18;
	nFieldWidth = 12;
}

void Configuration::SetupAssets()
{
	shapes[0].append(L"..X...X...X...X.");
	shapes[1].append(L"..X..XX..X......");
	shapes[2].append(L".X...XX...X.....");
	shapes[3].append(L".....XX..XX.....");
	shapes[4].append(L"..X..XX...X.....");
	shapes[5].append(L".....XX...X...X.");
	shapes[6].append(L".....XX..X...X..");
}

