#ifndef __ASCII_TETRIS_CONFIGURATION__08032020__
#define __ASCII_TETRIS_CONFIGURATION__08032020__
#include <string>

struct Configuration
{
	std::wstring shapes[7];
	int nScreenWidth;
	int nScreenHeight;
	int nFieldWidth;
	int nFieldHeight;

	Configuration();
	void SetupAssets();
};

#endif


