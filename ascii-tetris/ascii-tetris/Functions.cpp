#include "Functions.h"

namespace functions 
{

	int Rotate(int px, int py, int rotation)
	{
		switch (rotation % 4)
		{
			case 0: return  py * 4 + px;
			case 1: return  12 + py - (4 * px);
			case 2: return  15 - (py * 4) - px;
			case 3: return  3 - py + (4 * px);
			default: return 0;
		}
	}

	bool DoesFigureFit(const Configuration& cfg, const std::unique_ptr<unsigned char[]>& pTable, size_t nFigureId, int nRotation, int nPosX, int nPosY)
	{
		for (int px = 0; px < 4; ++px)
		{
			for (int py = 0; py < 4; ++py)
			{
				int pi = Rotate(px, py, nRotation);

				int fi = (nPosY + py) * cfg.nFieldWidth + (nPosX + px);

				if (nPosX + px >= 0 && nPosX + px < cfg.nFieldWidth)
				{
					if (nPosY + py >= 0 && nPosY + py < cfg.nFieldHeight)
					{
						if (cfg.shapes[nFigureId][pi] != L'.' && pTable[fi] != 0)
							return false;
					}
				}
			}
		}

		return true;
	}
}
